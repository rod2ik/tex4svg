from tex2svg import tex2svg

tex = r"""\begin{tikzpicture}
  \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
 \tkzTabLine{, +,} 
 \tkzTabVar{-/ , +/ }
 \tkzTabVal{1}{2}{0.3}{$0$}{1} % On place 1, son antécédent est 0.
 \tkzTabVal{1}{2}{0.6}{$1$}{e} % On place 1, son antécédent est 0.
\end{tikzpicture}"""

# print("tex =", tex)
svg = tex2svg(tex, mode="inline")

print("svg=\n", svg)